package controllers;

import models.ObdData;
import play.data.Form;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

public class Application extends Controller {

	static Form<ObdData> obdForm = Form.form(ObdData.class);

	public Result index() {
		return ok(index.render("This is mobil exper index page"));
	}

	/**
	 * Add the content-type json to response
	 *
	 * @param Result
	 *            httpResponse
	 *
	 * @return Result
	 */
	public Result jsonResult(Result httpResponse) {
		response().setContentType("application/json; charset=utf-8");
		return httpResponse;
	}

	public F.Promise<Result> pushData() {
		F.RedeemablePromise<Void> promise = F.RedeemablePromise.empty();

		Form<ObdData> requestForm = obdForm.bindFromRequest();
		if (requestForm.hasErrors()) {
			return promise.map((Void response) -> jsonResult(badRequest(requestForm.errorsAsJson())));
		}

		requestForm.get().save((Void t) -> promise.success(t));

		return promise.map((Void response) -> jsonResult(ok()));
	}

}
