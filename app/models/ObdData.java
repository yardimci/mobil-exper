/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;

import com.mongodb.async.client.MongoCollection;

/**
 * @author onur
 *
 */
public class ObdData {

	public String email;
	
	public List<ObdCommand> commandList;

	public Date createTime;

	public Double latitude;

	public Double longtitude;

	public String vin;
	
	public String name;

	private MongoCollection<Document> collection = MongoDB.database.getCollection("obddata", Document.class);

	public void save(Consumer<Void> func) {
		collection.insertOne(this.toDocument(), (Void result, Throwable t) -> func.accept(result));
	}
	
	public Document toDocument() {
		Document document = new Document("email", this.email)
				.append("createTime", createTime)
				.append("latitude", latitude)
				.append("longtitude", longtitude)
				.append("vin", vin)
				.append("name", name);
		List<Document> commands = new ArrayList<>();
		for (ObdCommand comm : commandList) {
			commands.add(comm.toDocument());
		}
		document.append("commands", commands);
		return document;
	}
}
