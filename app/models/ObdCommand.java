/**
 * 
 */
package models;

import org.bson.Document;

/**
 * @author onur
 *
 */
public class ObdCommand {
	
	public String mode;
	
	public String pid;
	
	public String value;
	
	public Document toDocument() {
		return new Document("mode", this.mode).append("pid", this.pid).append("value", value);
	}

}
