/**
 * 
 */
package models;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;

import play.Play;


/**
 * @author onur
 *
 */
public class MongoDB {
	
	public final static MongoDatabase database;

	static {
		MongoClient mongoClient = MongoClients.create(Play.application().configuration().getString("mongodb.connectionUri"));
		database = mongoClient.getDatabase(Play.application().configuration().getString("mongodb.database"));
	}

}
